import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TopbarComponent } from './topbar/topbar.component';
import { UserItemComponent } from './user-item/user-item.component';
import { Users } from './types';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [
    RouterOutlet,
    CommonModule,
    FormsModule,
    SidebarComponent,
    TopbarComponent,
    UserItemComponent,
  ],
})
export class AppComponent {
  // users: Users[] = [];

  users: Users[] = [
    {
      id: Math.random(),
      name: 'Amrit',
      email: 'amrit@gmail.com',
      designation: 'Developer',
      image:
        'https://cdn.iconscout.com/icon/free/png-256/free-laptop-user-1-1179329.png?f=webp',
      date: new Date('04/04/2024'),
      type: 'Admin',
      height: 180,
      countryCode: "+91",
      contact: 9876543210,
    },
    {
      id: Math.random(),
      name: 'John',
      email: 'john@gmail.com',
      designation: 'Analyst',
      image:
        'https://cdn.iconscout.com/icon/free/png-256/free-laptop-user-1-1179329.png?f=webp',
      date: new Date('04/04/2024'),
      type: 'User',
      height: 200,
      countryCode: "+91",
      contact: 9876543210
    },
    {
      id: Math.random(),
      name: 'John',
      email: 'john@gmail.com',
      designation: 'Analyst',
      image:
        'https://cdn.iconscout.com/icon/free/png-256/free-laptop-user-1-1179329.png?f=webp',
      date: new Date('04/04/2024'),
      type: 'Admin',
      height: 200,
      countryCode: "+91",
      contact: 9876543210
    },
    {
      id: Math.random(),
      name: 'John',
      email: 'john@gmail.com',
      designation: 'Analyst',
      image:
        'https://cdn.iconscout.com/icon/free/png-256/free-laptop-user-1-1179329.png?f=webp',
      date: new Date('04/04/2024'),
      type: 'User',
      height: 200,
      countryCode: "+91",
      contact: 9876543210
    },
  ];
  searchTerm: string = '';
  userCount = this.users.length;
  isFormRendered: boolean = false;
  addUser(newUser: Users): void {
    this.users.push(newUser);
    this.userCount = this.users.length;
  }

  selectedUser: Users | null = null;
  selectedUserDelete: Users | null = null;
  @Input() data: Users[] = [];
  handleAddFormEvent(isRender: boolean) {
    console.log('Form rendering status:', isRender);
    this.isFormRendered = isRender;
  }

  onUserSelected(user: Users) {
    console.log(user);
    this.selectedUser = user;
  }

  onUserSelectedDelete(user: Users) {
    console.log(user);
    this.selectedUserDelete = user;
    this.deleteUser(user.id);
  }

  deleteUser(userId: number): void {
    this.users = this.users.filter((user) => user.id !== userId);
    this.userCount = this.users.length;
  }

  updateUser(user: Users) {
    this.users = this.users.map((data) => (data.id === user.id ? user : data));
  }
}
