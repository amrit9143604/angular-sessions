import { Component, EventEmitter, Input, Output } from '@angular/core';
import { featherEdit } from '@ng-icons/feather-icons';
import { matDelete } from '@ng-icons/material-icons/baseline';
import { NgIcon, provideIcons } from '@ng-icons/core';
import { Users } from '../../types';
import { CommonModule, DatePipe } from '@angular/common';
import { ContactPipe } from './contact.pipe';
import { HoverDirective } from '../../hover.directive';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [NgIcon, CommonModule, ContactPipe, HoverDirective],
  templateUrl: './user.component.html',
  styleUrl: './user.component.scss',
  viewProviders: [provideIcons({ featherEdit, matDelete })],
  providers: [DatePipe],
})
export class UserComponent {
  @Input() user!: Users;
  @Input() selectedUser: Users | null = null;

  @Output() userSelected = new EventEmitter<any>();
  @Output() userSelectedDelete = new EventEmitter<any>();

  selectUser(user: Users): void {
    // console.log(user);
    this.userSelected.emit(user);
  }

  deleteUser(user: Users): void {
    // console.log(user);
    this.userSelectedDelete.emit(user);
  }

  getContactWithCountryCode(contact: number, countryCode: string): string {
    return `${countryCode} ${contact}`;
  }
}
