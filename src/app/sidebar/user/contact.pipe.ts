import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contact',
  standalone: true,
})
export class ContactPipe implements PipeTransform {
  transform(contact: number, countryCode: string): string {
    return `${countryCode} ${contact}`;
  }
}
