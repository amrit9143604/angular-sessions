import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UserComponent } from './user/user.component';
import { Users } from '../types';
import { CommonModule } from '@angular/common';
import { SearchPipe } from './search.pipe';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-sidebar',
    standalone: true,
    templateUrl: './sidebar.component.html',
    styleUrl: './sidebar.component.scss',
    imports: [UserComponent, CommonModule, FormsModule, SearchPipe]
})
export class SidebarComponent {
  @Input() users: Users[] = [];
  @Input() selectedUser: Users | null = null;
  @Output() userSelected = new EventEmitter<Users>();
  @Output() userSelectedDelete = new EventEmitter<Users>();
  searchTerm: string = '';

  constructor() {
  }
  
  onUserSelected(user: Users) {
    this.userSelected.emit(user);
  }

    
  onUserSelectedDelete(user: Users) {
    this.userSelectedDelete.emit(user);
  }
}
