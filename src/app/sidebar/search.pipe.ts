import { Pipe, PipeTransform } from '@angular/core';
import { Users } from '../types';

@Pipe({
  name: 'search',
  standalone: true
})
export class SearchPipe implements PipeTransform {
  transform(users: Users[], searchTerm: string): Users[] {
    if (!users || !searchTerm) {
      return users;
    }
    searchTerm = searchTerm.toLowerCase();
    return users.filter(user =>
      user.name.toLowerCase().includes(searchTerm) ||
      user.email.toLowerCase().includes(searchTerm) ||
      user.designation.toLowerCase().includes(searchTerm)
    );
  }
}