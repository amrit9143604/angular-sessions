export interface Users{
    id: number | 0;
    name: string | "";
    email: string | "";
    designation: string | "";
    image: string | "";
    date: Date | null;
    type: string | "";
    height: number | 0;
    contact: number | 0;
    countryCode: string | "";
}

export interface Country{
   country: string | '';
   code: string | '';
   dial_code: string | ''
}