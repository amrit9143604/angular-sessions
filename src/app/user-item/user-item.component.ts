import { CommonModule, DatePipe } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Country, Users } from '../types';
import { HoverDirective } from '../hover.directive';

@Component({
  selector: 'app-user-item',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule, FormsModule, HoverDirective],
  templateUrl: './user-item.component.html',
  styleUrl: './user-item.component.scss',

})
export class UserItemComponent implements OnChanges {
  id: number | null = null;
  email: string = '';
  name: string = '';
  designation: string = '';
  date: Date | null = null;
  height: number = 0;
  type: string = '';
  contact: number = 0;
  countryCode: string = '';
  nameError: string = '';
  emailError: string = '';
  designationError: string = '';
  dateError: string = '';
  heightError: string = '';
  typeError: string = '';
  contactError: string = '';
  countryCodeError: string = '';
  users: Users[] = [];
  @Input() user: Users | null = null;
  isEditMode: boolean = true;

  country: Country[] = [
    { country: 'United States', code: 'US', dial_code: '+1' },
    { country: 'United Kingdom', code: 'GB', dial_code: '+44' },
    { country: 'Canada', code: 'CA', dial_code: '+1' },
    { country: 'Australia', code: 'AU', dial_code: '+61' },
    { country: 'Germany', code: 'DE', dial_code: '+49' },
    { country: 'France', code: 'FR', dial_code: '+33' },
    { country: 'Japan', code: 'JP', dial_code: '+81' },
    { country: 'Brazil', code: 'BR', dial_code: '+55' },
    { country: 'China', code: 'CN', dial_code: '+86' },
    { country: 'India', code: 'IN', dial_code: '+91' },
    { country: 'Russia', code: 'RU', dial_code: '+7' },
    { country: 'South Africa', code: 'ZA', dial_code: '+27' },
    { country: 'Mexico', code: 'MX', dial_code: '+52' },
    { country: 'Italy', code: 'IT', dial_code: '+39' },
    { country: 'Spain', code: 'ES', dial_code: '+34' },
    { country: 'Argentina', code: 'AR', dial_code: '+54' },
    { country: 'South Korea', code: 'KR', dial_code: '+82' },
    { country: 'Saudi Arabia', code: 'SA', dial_code: '+966' },
    { country: 'Turkey', code: 'TR', dial_code: '+90' },
    { country: 'Indonesia', code: 'ID', dial_code: '+62' },
  ];

  @Output() addUserEvent = new EventEmitter<Users>();
  @Output() deleteUserEvent = new EventEmitter<number>();
  @Output() updateUserEvent = new EventEmitter<Users>();

  @Input() isFormRendered: boolean = true;

  ngOnChanges(changes: SimpleChanges): void {

    this.name = this.user?.name || '';
    this.email = this.user?.email || '';
    this.designation = this.user?.designation || '';
    this.date = this.user?.date || null;
    this.type = this.user?.type || '';
    this.contact = this.user?.contact || 0;
    this.countryCode = this.user?.countryCode || '';
    this.height = this.user?.height || 0;

    if (this.user == null) {
      this.isEditMode = true;
    } else {
      this.isEditMode = false;
    }
  }

  toggleEditMode() {
    this.isEditMode = true;
  }

  validateFields(): boolean {
    let isValid = true;

    this.nameError = '';
    this.emailError = '';
    this.designationError = '';
    this.dateError = '';
    this.heightError = '';
    this.typeError = '';
    this.contactError = '';
    this.countryCodeError = '';

    if (!this.name) {
      this.nameError = 'Name is required';
      isValid = false;
    }

    if (!this.email) {
      this.emailError = 'Email is required';
      isValid = false;
    } else if (!this.validateEmail(this.email)) {
      this.emailError = 'Invalid email format';
      isValid = false;
    }

    if (!this.designation) {
      this.designationError = 'Designation is required';
      isValid = false;
    }

    if (!this.date) {
      this.dateError = 'Date is required';
      isValid = false;
    }

    if (!this.type) {
      this.typeError = 'Type is required';
      isValid = false;
    }

    if (!this.height) {
      this.heightError = 'Height is required';
      isValid = false;
    } else if (this.height < 0) {
      this.heightError = 'Height cannot be negative';
      isValid = false;
    }

    if (!this.contact) {
      this.contactError = 'Contact is required';
      isValid = false;
    }

    if (!this.countryCode) {
      this.countryCodeError = 'Country Code is required';
      isValid = false;
    }

    return isValid;
  }

  validateEmail(email: string): boolean {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  }

  addUser(): void {
    if (!this.validateFields()) {
      return;
    }
    const newUser: Users = {
      id: Math.random(),
      name: this.name || '',
      email: this.email || '',
      designation: this.designation || '',
      date: this.date !== null ? this.date : new Date(),
      height: this.height || 0,
      type: this.type || '',
      contact: this.contact || 0,
      countryCode: this.countryCode || '',
      image:
        'https://cdn.iconscout.com/icon/free/png-256/free-laptop-user-1-1179329.png?f=webp',
    };

    this.addUserEvent.emit(newUser);
    this.resetForm();
    // console.log(this.user);
    if (this.user == null) {
      this.isEditMode = true;
    } else {
      this.isEditMode = false;
    }
  }

  resetForm(): void {
    this.name = '';
    this.email = '';
    this.designation = '';
    this.height = 0;
    this.type = '';
    this.contact = 0;
    this.countryCode = '';
    this.date = new Date();
    this.isEditMode = false;
  }

  deleteUser(): void {
    if (this.user && this.user.id) {
      this.deleteUserEvent.emit(this.user.id);
      this.resetForm();
      this.isEditMode = true;
      this.user = null;
      this.isFormRendered = false;
    }
  }
  updateUser(): void {
    if (this.user) {
      if (!this.validateFields()) {
        return;
      }
      // console.log(this.user);
      const updatedUser: Users = {
        id: this.user.id,
        name: this.name || '',
        email: this.email || '',
        designation: this.designation || '',
        date: this.date !== null ? this.date : new Date(),
        height: this.height || 0,
        type: this.type || '',
        contact: this.contact || 0,
        countryCode: this.countryCode || '',
        image:
          'https://cdn.iconscout.com/icon/free/png-256/free-laptop-user-1-1179329.png?f=webp',
      };
      this.updateUserEvent.emit(updatedUser);
      this.resetForm();
      this.isEditMode = true;
      this.user = null;
    }
  }
}
