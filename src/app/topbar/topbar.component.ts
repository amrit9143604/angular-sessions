import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-topbar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './topbar.component.html',
  styleUrl: './topbar.component.scss',
})
export class TopbarComponent {
  isRender: boolean = false;
  @Output() addFormEvent = new EventEmitter<boolean>();


  renderForm() {
    this.isRender = true;
    this.addFormEvent.emit(this.isRender);  
  }
}
