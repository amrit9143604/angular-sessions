import { Injectable } from '@angular/core';
import { Users } from './types';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private users: Users[] = [];

  constructor() {}

  getUsers(): Users[] {
    return this.users;
  }

  addUser(newUser: Users): void {
    this.users.push(newUser);
  }

  deleteUser(userId: number): void {
    this.users = this.users.filter((user) => user.id !== userId);
  }

  updateUser(updatedUser: Users): void {
    const index = this.users.findIndex((user) => user.id === updatedUser.id);
    if (index !== -1) {
      this.users[index] = { ...updatedUser };
    }
  }
}
